<?php

namespace Classes;

class StrikeCommand extends \Ahc\Cli\Input\Command
{
    private $strikeDeploy;

    /**
     * StrikeCommand constructor.
     * @param StrikeDeploy $deploy
     */
    public function __construct(StrikeDeploy $deploy)
    {
        parent::__construct('strike', 'Empire Strikes Back');
        $this->strikeDeploy = $deploy;
    }

    /**
     * @param \Ahc\Cli\IO\Interactor $io
     */
    public function interact(\Ahc\Cli\IO\Interactor $io)
    {
        $confirm = $io->confirm('Strike now Emperor?', 'n');

        if (!$confirm) {
            exit();
        }
    }


    public function execute()
    {
        $io = $this->app()->io();
        $this->strikeDeploy->deployDrones($io);
        $io->write('Perimeter Secured ', true);
        $io->write("drones deployed: " . $this->strikeDeploy->getDronesDeployed() . "\n");
        $io->write("drones crashed: " . $this->strikeDeploy->getDronesCrashed() . "\n");
        $io->write("Flight path: " . $this->strikeDeploy->getRequestPath() . "\n");
        exit();
    }
}