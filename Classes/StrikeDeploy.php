<?php

namespace Classes;

use GuzzleHttp\Client;
use Interfaces\RequestInterface;

class StrikeDeploy implements RequestInterface
{
    protected $client;
    protected $baseUrl;
    protected $name = "Michael Coyne";
    protected $validPaths = [];
    protected $invalidPaths = [];
    protected $dronesDeployed = 0;
    protected $dronesCrashed = 0;
    protected $directions = ['l', 'r', 'f'];

    const OK = 410;
    const CRASH = 417;
    const SUCCESS = 200;

    /**
     * StrikeDeploy constructor.
     */
    public function __construct()
    {
        $this->baseUrl = 'https://deathstar.dev-tests.vp-ops.com/empire.php';
        $this->client = new Client();
    }

    /**
     * @param string $path
     * @return \GuzzleHttp\Psr7\Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function request(string $path): \GuzzleHttp\Psr7\Response
    {
        return $this->client->request('GET', $this->baseUrl,
            [
                'query' => [
                    'name' => $this->name,
                    'path' => $path
                ],

            ]);
    }

    /**
     * @param string $currentPath
     * @param int $attempt
     * @return string
     */
    protected function generatePath(string $currentPath = '', int $attempt = 0)
    {
        $lastDirection = substr($currentPath, -1);
        $check = !isset($this->invalidPaths[$currentPath . 'f']) ? $currentPath . 'f' : false;

        if ($check && !isset($this->invalidPaths[$check])) {
            $newPath = $currentPath . 'f';
        } else {

            if ($currentPath === '') {
                $randomDirection = array_rand($this->directions);
                $newPath = $currentPath . $this->directions[$randomDirection];
            } else {
                // aim to go forward first
                if ($lastDirection === 'f') {
                    $directions = ['r', 'l'];
                    $randomDirection = array_rand($directions);
                    $newPath = $currentPath . $directions[$randomDirection];
                } else {
                    // don't go back on yourself
                    $newPath = $currentPath . $lastDirection;
                }
            }
        }

        return $newPath;
    }

    /**
     * @param \Ahc\Cli\IO\Interactor $io
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function deployDrones(\Ahc\Cli\IO\Interactor $io)
    {
        $move = 0;
        $path = '';

        do {

            $this->dronesDeployed++;
            $move++;

            if ($move === 1 && $this->dronesDeployed === 1) {
                $path = $this->generatePath();
            } else {

                if (!empty($this->validPaths)) {

                    $validPath = end($this->validPaths);
                    $path = $this->generatePath($validPath);

                    if (isset($this->invalidPaths[$path])) {
                        array_pop($this->validPaths);
                        $invalidPath = end($this->validPaths);
                        $this->invalidPaths = [$invalidPath];
                        continue;
                    }

                } else {
                    $path = $this->generatePath($path);
                }
            }

            $response = $this->deployDrone($path);
            $io->write($path . "\n");

            if ($response === self::CRASH) {
                $this->invalidPaths[$path] = $path;
                $this->dronesCrashed++;
                continue;
            }

            if ($response === self::OK) {
                $this->validPaths[$path] = $path;
                continue;
            }

        } while ($response !== self::SUCCESS);
    }

    /**
     * @param $path
     * @return int
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function deployDrone($path)
    {
        try {
            $response = $this->request($path);
        } catch (\GuzzleHttp\Exception\ClientException $exception) {
            $response = $exception->getResponse();
        }

        return $response->getStatusCode();
    }

    /**
     * @return int
     */
    public function getDronesDeployed(): int
    {
        return $this->dronesDeployed;
    }

    /**
     * @return int
     */
    public function getDronesCrashed(): int
    {
        return $this->dronesCrashed;
    }

    /**
     * @return string
     */
    public function getRequestPath() :string {
        return end($this->validPaths);
    }
}