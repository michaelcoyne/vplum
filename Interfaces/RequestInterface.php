<?php

namespace Interfaces;

interface RequestInterface
{
    public function request(string $path);
}