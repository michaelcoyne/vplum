<?php

require "vendor/autoload.php";
use \Classes\StrikeCommand;
use Classes\StrikeDeploy;
ini_set('xdebug.max_nesting_level', 50000);

// Init App with name and version
$app = new \Ahc\Cli\Application('App', 'v0.0.1');
$strikeDeploy = new StrikeDeploy();
// Add commands with optional aliases`
$app->add(new StrikeCommand($strikeDeploy), 's');

// Set logo
$app->logo("
  ___| |_ __ _ _ __  __      ____ _ _ __ ___ 
 / __| __/ _` | '__| \ \ /\ / / _` | '__/ __|
 \__ \ || (_| | |     \ V  V / (_| | |  \__ \
 |___/\__\__,_|_|      \_/\_/ \__,_|_|  |___/

.-.__      \ .-.  ___  __
|_|  \'--.-.-(   \/\;;\_\.-._______.-.
(-)___     \ \ .-\ \;;\(   \       \ \
 Y    \'---._\_((Q)) \;;\\ .-\     __(_)
 I           __\'-\' / .--.((Q))---\'    \,
 I     ___.-:    \|  |   \\' - \'_          \
 A  .-\'      \ .-.\   \   \ \ \'--.__     \'\
 |  |____.----((Q))\   \__|--\_      \     \'
    ( )        \'-\'  \_  :  \-\' \'--.___\
     Y                \  \  \       \(_)
     I                 \  \  \         \,
     I                  \  \  \          \
     A                   \  \  \          \'\
     |                    \  \__|           \'
                           \_:.  \
                             \ \  \
                              \ \  \
                               \_\_|");

$app->handle($_SERVER['argv']); // if argv[1] is `i` or `init` it executes StrikeCommand